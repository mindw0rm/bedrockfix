package net.kaoztribe.bedrockfix;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkPopulateEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class BedrockFix extends JavaPlugin implements Listener {

  List<String> worlds;
  boolean      force;
  Path         logPath;

  int          logLevel, fixedChunks;

  String       discordURL, discordSecret;

  final List<FixTask>  tasks = new ArrayList<>();

  @Override
  public void onEnable() {

    saveDefaultConfig();

    worlds = getConfig().getStringList("worlds");
    force  = getConfig().getBoolean("force");

    logPath = Paths.get("logs/bedrockfix.log");
    logLevel = getConfig().getInt("loglevel", 1);
    fixedChunks = getConfig().getInt("fixedChunks", 0);
    discordURL = getConfig().getString("discord.url");
    discordSecret = getConfig().getString("discord.secret");
    try {
      Files.delete(logPath);
    }catch (NoSuchFileException x) {
    }
    catch (IOException e) {
      e.printStackTrace();
      Bukkit.shutdown();
      return;
    }
    for (String worldName : worlds) {
      tasks.add(new FixTask(this, worldName));
    }
    // Bukkit.getServer().getPluginManager().registerEvents(this, this);
  }

  @Override
  public void onDisable() {
    tasks.forEach(task -> task.cancelAndWait());
  }

  /*
  @EventHandler(priority = EventPriority.LOWEST)
  public void onChunkLoadLowest(ChunkLoadEvent e) {
    if (worlds.contains(e.getChunk().getWorld().getName()))
      log(0, String.format("ChunkLoad(%d, %d) [lowest]: %s", e.getChunk().getX(), e.getChunk().getZ(), e.getChunk().getBlock(0, -1, 0).getType().name()));
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void onChunkLoadHighest(ChunkLoadEvent e) {
    if (worlds.contains(e.getChunk().getWorld().getName()))
      log(0, String.format("ChunkLoad(%d, %d) [highest]: %s", e.getChunk().getX(), e.getChunk().getZ(), e.getChunk().getBlock(0, -1, 0).getType().name()));
  }
  @EventHandler(priority = EventPriority.MONITOR)
  public void onChunkLoadMonitor(ChunkLoadEvent e) {
    if (worlds.contains(e.getChunk().getWorld().getName()))
      log(0, String.format("ChunkLoad(%d, %d) [monitor]: %s", e.getChunk().getX(), e.getChunk().getZ(), e.getChunk().getBlock(0, -1, 0).getType().name()));
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onChunkPopulateLo(ChunkPopulateEvent e) {
    if (worlds.contains(e.getChunk().getWorld().getName()))
      log(0, String.format("ChunkPopulate(%d, %d) [lowest]: %s", e.getChunk().getX(), e.getChunk().getZ(), e.getChunk().getBlock(0, -1, 0).getType().name()));
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void onChunkPopulateHi(ChunkPopulateEvent e) {
    if (worlds.contains(e.getChunk().getWorld().getName()))
      log(0, String.format("ChunkPopulate(%d, %d) [highest]: %s", e.getChunk().getX(), e.getChunk().getZ(), e.getChunk().getBlock(0, -1, 0).getType().name()));
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onChunkPopulateMo(ChunkPopulateEvent e) {
    if (worlds.contains(e.getChunk().getWorld().getName()))
      log(0, String.format("ChunkPopulate(%d, %d) [monitor]: %s", e.getChunk().getX(), e.getChunk().getZ(), e.getChunk().getBlock(0, -1, 0).getType().name()));
  }
  */

  public void log(int level, String s) {
    synchronized (this) {
      if (level <= logLevel) {
        Bukkit.getConsoleSender().sendMessage("§3[BedrockFix] §5" + s);
      }
      try {
        String str = s + "\n";

        Files.write(logPath, str.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
      }
      catch (Exception e) {
        getLogger().warning("ERROR logging " + s);
        e.printStackTrace();
      }
    }
  }

  public void discordMsg(String msg) {
    if (discordURL != null && !discordURL.isEmpty() && discordSecret != null && !discordSecret.isEmpty()) {
      try {
        String content = String.format("message=%s&secret=%s",
                                       URLEncoder.encode(msg, StandardCharsets.UTF_8.toString()),
                                       URLEncoder.encode(discordSecret, StandardCharsets.UTF_8.toString()));
        URL    url = new URL(discordURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-length", String.valueOf(content.length()));
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setDoOutput(true);
        connection.setDoInput(true); // bot gives no response, but required for getResponse

        DataOutputStream output = new DataOutputStream(connection.getOutputStream());

        output.writeBytes(content);
        output.close();

        String answer = "Calling the Discord BOT API returned " + connection.getResponseCode() + " " + connection.getResponseMessage();
        if (connection.getResponseCode() != 200) {
          throw new RuntimeException(answer);
        }
      }
      catch (Exception e) {
        log(0, "§4ERROR sending discord message: " + e.getMessage());
        e.printStackTrace();
      }
    }
  }

  public void saveCfg(String key, Object value) {
    try {
      getConfig().set(key, value);
      getConfig().save(new File(getDataFolder(), "config.yml"));
    } catch (IOException e) {
      log(0, "§4ERROR saving config!");
      e.printStackTrace();
    }
  }

  public void incFixed() {
    fixedChunks++;
    saveCfg("fixedChunks", fixedChunks);
  }
}
