package net.kaoztribe.bedrockfix;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

public class ChunkFixer {

  private final FixTask task;
  private final Chunk chunk;

  int x, z;
  public ChunkFixer(FixTask t, Chunk ch) {
    task = t;
    chunk = ch;
    x = z = 0;
    Bukkit.getScheduler().runTaskLater(task.plugin, () -> run(), 1);
  }

  public void run() {
    int   allowed = 64;  // ???
    Block b;

    while (!task.abort && allowed > 0 && x < 16) {
      while (!task.abort && allowed > 0 && z < 16) {
        b = chunk.getBlock(x, task.minY, z);
        b.setType(Material.BEDROCK);
        z++;
        allowed--;
      }
      x++;
      z = 0;
    }
    if (x < 16) {
      // not yet done: run again
      if (!task.abort)
        Bukkit.getScheduler().runTaskLater(task.plugin, () -> run(), 1);
    }
    else {
      task.plugin.log(2, "    -> chunk done!");
      task.plugin.incFixed();
      chunk.setForceLoaded(false);
      chunk.unload(true);
      task.busy = false;
    }
  }

}
