package net.kaoztribe.bedrockfix;

import io.papermc.lib.PaperLib;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.Arrays;

import static java.lang.Thread.sleep;

public class FixTask extends BukkitRunnable {

  final BedrockFix plugin;
  final String worldName;

  boolean busy, abort;
  int minY;
  public FixTask(BedrockFix pl, String name) {
    plugin    = pl;
    worldName = name;
    busy      = false;
    abort     = false;
    // start Task:
    runTaskLaterAsynchronously(plugin, 500);
  }

  public void cancelAndWait() {
    long start = System.currentTimeMillis();

    abort = true;
    cancel();
    // multiple chunks can be done in a second, so waiting shouldn'T take longer than 1s ...
    while (!isCancelled() && System.currentTimeMillis() - start < 1000) {
      try {
        sleep(20);
      }
      catch (InterruptedException e) {
        // ignore
      }
    }
    if (!isCancelled())
      plugin.log(0, String.format("§4Cancelling the task for %s takes too long.", worldName));
  }

  class Abort extends RuntimeException {}

  @Override
  public void run() {
    long start = System.currentTimeMillis();
    plugin.log(0, "***** Starting bedrock fix for world " + worldName + " *****");
    plugin.discordMsg("***** Bedrock fix für " + worldName + " gestartet *****");
    try {
      World world = Bukkit.getWorld(worldName);


      if (world == null) {
        throw new Exception("world not found");
      }
      minY = world.getMinHeight();

      // now iterate over all region files
      File         rgFolder = new File(worldName + "/region");
      File[]       regions = rgFolder.listFiles();

      Arrays.stream(regions).filter(rg -> rg.isFile()).forEach(rg -> {
        String[] parts = rg.getName().split("\\.");
        int      rgX = Integer.parseInt(parts[1]), rgZ = Integer.parseInt(parts[2]);
        String   doneKey = String.format("doneRegions.r_%d_%d", rgX, rgZ).replace('-', '_');

        if (!plugin.getConfig().getBoolean(doneKey, false)) {
          int      x, z, minX = rgX * 32, maxX = minX + 31, minZ = rgZ * 32, maxZ = minZ + 31;
          long     chunkStart;
          boolean  checkTime;

          plugin.log(1, String.format("working on region %d,%d", rgX, rgZ));
          for (x = minX; !abort && x <= maxX; x++) {
            for (z = minZ; !abort && z <= maxZ; z++) {
              // fixChunk runs in the main thread, so her hve to wait till that is done!
              busy = true;
              checkTime = true;
              chunkStart = System.currentTimeMillis();
              plugin.log(3, String.format(" - requesting chunk %d,%d", x, z));
              if (!abort)
                PaperLib.getChunkAtAsync(world, x, z, false, true).thenAccept(ch -> fixChunk(ch));
              while (busy && !abort) {
                try {
                  sleep(100);
                } catch (Exception e) {
                }
                // a chunk _should_ Be done in less than 10 ticks, i.e. far less than a second.
                if (checkTime && System.currentTimeMillis() - chunkStart > 10000) {
                  plugin.log(0, "§4    -> Processing of chunk takes too long!");
                  plugin.discordMsg("Bedrock fix steht, bitte Server neu starten!");
                  checkTime = false;
                  throw new Abort();
                }
              }
            }
          }
          if (abort)  // might not be fully done -> do not save as done!
            throw new Abort();
          plugin.saveCfg(doneKey, true);
          plugin.log(1, String.format("region %d,%d done", rgX, rgZ));
        }
        else {
          plugin.log(1, String.format("region %d,%d already done.", rgX, rgZ));
        }
      });
    }
    catch (Abort a) {
      // not an error
    }
    catch (Exception e) {
      plugin.log(0, "ERROR in world " + worldName + ": " + e.getMessage());
      e.printStackTrace();
    }
    plugin.log(0, "***** bedrock fix for world " + worldName + " " + (isCancelled() ? "aborted" : "finished") +  " in "
            + ((System.currentTimeMillis() - start) / 1000) + " seconds *****");
    plugin.discordMsg("***** Bedrock fix für " + worldName + " beendet *****");
  }

  private void fixChunk(Chunk ch) {
    if (ch == null) {  // this happens, most likely if the chunk is not yet generated
      plugin.log(3,"   -> not generated");
      busy = false;
    }
    else {
      plugin.log(3, String.format("    -> got chunk, processing ..."));
      // check if there si anything to do:
      if (plugin.force || ch.getBlock(0, minY, 0).getType() != Material.BEDROCK) {
        // we need to fix this chunk!
        ch.setForceLoaded(true);
        plugin.log(1, String.format("    -> chunk %d,%d needs fixing!", ch.getX(), ch.getZ()));
        new ChunkFixer(this, ch);
      }
      else {
        ch.unload();
        busy = false;
      }
    }
  }
}
